const dotenv = require('dotenv');
dotenv.config({ path: process.env.CONF + '/.env' });
console.log(process.env.DB_USER);

module.exports = {
    //Config DATABASE
    database: {
        user: process.env.DB_USER,
        database: process.env.DB_DATABASE,
        password: process.env.DB_PASSWORD,
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        poolMax: process.env.DB_POOL_MAX
    }
}