const pg = require('pg'),
    config = require('./../config');
const pool = new pg.Pool({
    user: config.database.user, //env var: PGUSER
    database: config.database.database, //env var: PGDATABASE
    password: config.database.password, //env var: PGPASSWORD
    host: config.database.host, // Server
    port: config.database.port, //env var: PGPORT
    max: config.database.poolMax //number of connections to use in connection pool
});
pool.on('error', (err, client) => {
    console.error(`Unexpected error on idle client: ${err}`);
});