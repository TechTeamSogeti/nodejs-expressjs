"use strict"
const QueryStream = require('pg-query-stream');

const pool = require('./../database/connection');

pool.on('error', (err) => {
    console.log('idle client error', err.message, err.stack);
});

const queryBuilderCb = (queryText, values, cb) => {
    pool.connect((err, client, done) => {
        if (err) {
            done();
            console.log(err);
            console.log(`Connection released`);
            cb(err, null);
        } else {
            client.query(queryText, values, (err, result) => {
                done();
                console.log(`Connection released`);
                cb(err, result);
            });
        }
    });
};
const queryBuilderStream = (queryText, values) => {
    return new Promise((resolve, reject) => {
        pool.connect()
            .then(client => {
                const query = new QueryStream(queryText, values);
                const stream = client.query(query);
                client.release();
                console.log(`Connection released`);
                resolve(stream);
            })
            .catch(error => {
                console.log(error);
                reject({ error: error, code: 500 });
            });
    });
};

const queryBuilderPromise = (queryText, values) => {
    return new Promise(async(resolve, reject) => {
        try {
            const client = await pool.connect()
            const results = await client.query(queryText, values);
            resolve(results);
        } catch (error) {
            logger.error(error.stack);
            reject(error);
        } finally {
            client.release();
        }
    });
};

module.exports = {
    queryBuilderCb: queryBuilderCb,
    queryBuilderPromise: queryBuilderPromise,
    queryBuilderStream: queryBuilderStream
}