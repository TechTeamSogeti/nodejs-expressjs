"use strict";
const request = require('request'),
    HttpStatus = require('http-status-codes'),
    server = require('./../../../app');

jasmine.getEnv().defaultTimeoutInterval = 500;

const endpoint = 'http://localhost:3000/v1/users',
    customer_id = 25;

describe("customers", () => {
    describe(' / GET ONE', () => {
        it('should returns status code 200', (done) => {
            request.get(`${endpoint}/${customer_id}`, (error, response) => {
                expect(response.statusCode).toEqual(HttpStatus.OK);
                done();
            });
        });
    });
});