const express = require('express'),
    HttpStatus = require('http-status-codes'),
    router = express.Router();

const customersService = require('./usersService');

router.get('/users/:id', async(req, res, next) => {
    let customer_id = req.params.id;
    //put the custoemrsService here
    let data;
    try {
        data = await customersService.getOne(customer_id);
        let results = data.rows;
        if (result.rowCount.length === 0) {
            res.sendStatus(HttpStatus.NOT_FOUND);
        } else {
            res.status(HttpStatus.OK).send(results);
        }
    } catch (error) {
        res.sendStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        //next(error)
    }

});

module.exports = router