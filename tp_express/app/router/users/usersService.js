'use strict';
const db = require('./../../database/queryBuilder');

module.exports.getOne = (id_user) => {
    let sql = `SELECT * FROM customers where customer_id = $1`,
        values = [id_user];
    return db.queryBuilderPromise(sql, values);
};