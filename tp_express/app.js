const express = require('express'),
    bodyParser = require('body-parser');

const app = express();

const logger = require('./logger');
const conf = require('./app/config')
    //print in the console
logger.info('I use winston logger')
logger.error('I use winston logger')
const router = require('./router');

const usersRouter = require('./app/router/users/usersRouter');
app.use('/v1', usersRouter);
// parse application/json 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static('public'));
app.use('/v1', router)

let port = process.env.PORT || 3000;

app.get('/hello', (req, res) => {
    res.status(200).send('Hello with fwk express')
});


app.listen(port);