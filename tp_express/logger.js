const winston = require('winston');
//put log levels here
let logger = new(winston.Logger)({
    transports: [
        new winston.transports.Console({
            level: 'info',
            handleExceptions: true,
            humanReadableUnhandledException: true,
            timestamp: true,
            json: false,
            colorize: true
        }),
        new winston.transports.File({
            level: 'info',
            filename: "C:/Temp/all-logs.log",
            timestamp: true,
            handleExceptions: true,
            humanReadableUnhandledException: true,
            json: false,
            maxsize: 5242880, //5MB
            maxFiles: 5,
            colorize: false
        }),
    ]
});
module.exports = logger;