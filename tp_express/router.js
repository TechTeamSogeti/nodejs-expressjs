const express = require('express');

const router = express.Router();
const bodyParser = require('body-parser');

let port = process.env.PORT || 3000;

router.get('/router', (req, res) => {
    res.status(200).send('Hello with fwk express using classe Router')
});

router.post('/router', (req, res, next) => {
    let data = req.body
    console.log(`At this point we will insert data`);
    //res.status(201).send(data);
    res.sendStatus(201)
});

router.get('/users/:id', (req, res, next) => {
    let userId = req.params.id;
    //send result
    res.status(200).send(`The user id is: ${userId}`)
});

router.post('/users', (req, res, next) => {
    let firstname = req.body.firstname;
    let lastname = req.body.lastname;
    //send result
    res.status(201).send(`From the body i receive: Firstname: ${firstname} && Lastname: ${lastname}`)
});

router.put('/users?', (req, res, next) => {
    let userId = req.query.id;
    let userName = req.query.name;
    //send response
    res.status(201).send(`The user is: ${userName} with id=${userId}`)
});

module.exports = router;