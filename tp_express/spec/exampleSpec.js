const request = require('request'),
    HttpStatus = require('http-status-codes');

const server = require('./../app');

const options = {
    method: 'GET',
    uri: 'http://127.0.0.1:3000/v1/users/5'
};

describe('users', () => {
    describe('GET /', () => {
        it('should returns status code 200', (done) => {
            request.get(options,
                (error, response) => {
                    expect(response.statusCode).toEqual(HttpStatus.OK);
                    done();
                });
        });
    });
});